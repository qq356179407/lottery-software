# 抽奖软件

#### 介绍
抽奖软件，抽奖使用说明，支持各种活动抽奖

#### 抽奖软件使用说明


前言 - 此软件为免费软件供大家使用，部分图片来网络。


1.	解压抽奖到任意目录地址
目录文件如下所示

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/221638_a76c32c3_9183185.png "1.png")

2.	抽奖前我们需要配置抽奖人员名单以及奖项
打开Config.xlsx - 配置信息， 如下有三张表，黄色内容是必填项

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/221653_72e8fd2f_9183185.png "2.png")

a.	namelist – 配置人员名单，修改并添加人员名单。所有抽奖仅以此名单随机抽取。

b.	prizelist- 奖项设置，修改或添加奖项， 其中Count 是每项奖品人员名额，Seq是抽奖顺序


c.	title – 设置抽奖标题，如“某某公司2020新年晚会抽奖”

3.	添加图片头像jpg格式到目录HeadImages，头像图片命名必须和 人员名单图片名字一致，否则图片会显示为guest.jpg. 提示，guest.jpg图片不能删除可以替换成其他图片。

4.	运行“抽奖.exe”。点击“空格键”抽取，如需进入下一奖项徐鼠标点击对应按钮。 点击“ESC”退出软件。

5.	抽奖结果 会存入软件目录，如“![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/221740_489ef08e_9183185.png "result.png") ”。

6.	background.mp3 - 背景音乐可以替换成喜欢的音乐，但是格式以及名字需保持一致。

7.	bingo.wav - 中奖提示音乐换成喜欢的提示音，但是格式以及名字需保持一致。

8.	部分截图。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/221754_c2799d6d_9183185.png "3.png")
 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/221805_6a832c23_9183185.png "4.png")

 
 
