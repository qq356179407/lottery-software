﻿namespace Luckdraw
{
    partial class Form1
    {
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnBingo;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblName;
        private PanelEnhanced panel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panBingo;
        private System.Windows.Forms.Label lblBingo;
        private System.Windows.Forms.Label lblCurrentPrize;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel5 = new Luckdraw.PanelEnhanced();
            this.lblCurrentPrizeBigShow = new System.Windows.Forms.Label();
            this.lblCurrnetPrizeDescription = new System.Windows.Forms.Label();
            this.lblCurrentPrize = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panBingo = new System.Windows.Forms.Panel();
            this.lblBingo = new System.Windows.Forms.Label();
            this.btnBingo = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel5.SuspendLayout();
            this.panBingo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel5.BackgroundImage")));
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.lblCurrentPrizeBigShow);
            this.panel5.Controls.Add(this.lblCurrnetPrizeDescription);
            this.panel5.Controls.Add(this.lblCurrentPrize);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.panBingo);
            this.panel5.Controls.Add(this.btnBingo);
            this.panel5.Controls.Add(this.lblTitle);
            this.panel5.Controls.Add(this.btnNext);
            this.panel5.Controls.Add(this.lblName);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(939, 522);
            this.panel5.TabIndex = 5;
            // 
            // lblCurrentPrizeBigShow
            // 
            this.lblCurrentPrizeBigShow.AutoSize = true;
            this.lblCurrentPrizeBigShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentPrizeBigShow.Location = new System.Drawing.Point(297, 202);
            this.lblCurrentPrizeBigShow.Name = "lblCurrentPrizeBigShow";
            this.lblCurrentPrizeBigShow.Size = new System.Drawing.Size(0, 73);
            this.lblCurrentPrizeBigShow.TabIndex = 8;
            // 
            // lblCurrnetPrizeDescription
            // 
            this.lblCurrnetPrizeDescription.AutoSize = true;
            this.lblCurrnetPrizeDescription.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrnetPrizeDescription.ForeColor = System.Drawing.Color.Yellow;
            this.lblCurrnetPrizeDescription.Location = new System.Drawing.Point(284, 291);
            this.lblCurrnetPrizeDescription.Name = "lblCurrnetPrizeDescription";
            this.lblCurrnetPrizeDescription.Size = new System.Drawing.Size(0, 33);
            this.lblCurrnetPrizeDescription.TabIndex = 7;
            // 
            // lblCurrentPrize
            // 
            this.lblCurrentPrize.AutoSize = true;
            this.lblCurrentPrize.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentPrize.ForeColor = System.Drawing.SystemColors.Control;
            this.lblCurrentPrize.Location = new System.Drawing.Point(533, 427);
            this.lblCurrentPrize.Name = "lblCurrentPrize";
            this.lblCurrentPrize.Size = new System.Drawing.Size(49, 19);
            this.lblCurrentPrize.TabIndex = 5;
            this.lblCurrentPrize.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(664, 112);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(142, 56);
            this.panel1.TabIndex = 4;
            // 
            // panBingo
            // 
            this.panBingo.AutoScroll = true;
            this.panBingo.Controls.Add(this.lblBingo);
            this.panBingo.Location = new System.Drawing.Point(652, 174);
            this.panBingo.Name = "panBingo";
            this.panBingo.Size = new System.Drawing.Size(187, 240);
            this.panBingo.TabIndex = 3;
            // 
            // lblBingo
            // 
            this.lblBingo.AutoSize = true;
            this.lblBingo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBingo.ForeColor = System.Drawing.Color.White;
            this.lblBingo.Location = new System.Drawing.Point(14, 9);
            this.lblBingo.Name = "lblBingo";
            this.lblBingo.Size = new System.Drawing.Size(0, 20);
            this.lblBingo.TabIndex = 0;
            // 
            // btnBingo
            // 
            this.btnBingo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBingo.BackgroundImage")));
            this.btnBingo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBingo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBingo.ForeColor = System.Drawing.Color.Yellow;
            this.btnBingo.Location = new System.Drawing.Point(425, 424);
            this.btnBingo.Name = "btnBingo";
            this.btnBingo.Size = new System.Drawing.Size(102, 28);
            this.btnBingo.TabIndex = 2;
            this.btnBingo.Text = "开始";
            this.btnBingo.UseVisualStyleBackColor = false;
            this.btnBingo.Click += new System.EventHandler(this.button3_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.SystemColors.Window;
            this.lblTitle.Font = new System.Drawing.Font("Arial Rounded MT Bold", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Yellow;
            this.lblTitle.Location = new System.Drawing.Point(343, 41);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(324, 55);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "LUCK DRAW";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnNext
            // 
            this.btnNext.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNext.BackgroundImage")));
            this.btnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNext.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.Color.Yellow;
            this.btnNext.Location = new System.Drawing.Point(401, 424);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(106, 29);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "抽取下一奖项";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.Yellow;
            this.lblName.Location = new System.Drawing.Point(420, 361);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(17, 26);
            this.lblName.TabIndex = 2;
            this.lblName.Text = " ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(371, 144);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(189, 214);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(939, 522);
            this.Controls.Add(this.panel5);
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panBingo.ResumeLayout(false);
            this.panBingo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCurrnetPrizeDescription;
        private System.Windows.Forms.Label lblCurrentPrizeBigShow;

      
    }
}

