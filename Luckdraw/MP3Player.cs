﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luckdraw
{
   
    public class Mp3Player
    {
        /// <summary>   
        /// 文件地址   
        /// </summary>   
        public string FilePath;

        /// <summary>   
        /// 播放   
        /// </summary>   
        public void Play()
        {
            mciSendString("close all", "", 0, 0);
            mciSendString("open " + FilePath + " alias media", "", 0, 0);
            mciSendString("play media repeat", "", 0, 0);
        }

        /// <summary>   
        /// 暂停   
        /// </summary>   
        public void Pause()
        {
            mciSendString("pause media", "", 0, 0);
        }
        /// <summary>   
        /// 继续播放 
        /// </summary>   
        public void Continue()
        {
            mciSendString("resume media", "", 0, 0);
        }

        /// <summary>   
        /// 停止   
        /// </summary>   
        public void Stop()
        {
            mciSendString("close media", "", 0, 0);
        }

        /// <summary>   
        /// API函数   
        /// </summary>   
        [System.Runtime.InteropServices.DllImport("winmm.dll", EntryPoint = "mciSendString", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private static extern int mciSendString(
            string lpstrCommand,
            string lpstrReturnString,
            int uReturnLength,
            int hwndCallback
        );
    }
}
