﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luckdraw
{
    public class prize
    {
        public int SN { get; set; }
        public string PrizeName { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int Count { get; set; }
        public int Seq { get; set; }
    }

    public class name
    {
        public int SN { get; set; }
        public string Name { get; set; }
    }
    public class bingo
    {
        public string Name { get; set; }
        public  string PrizeName { get; set; }
    }

}
